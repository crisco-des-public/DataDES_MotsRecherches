# DataDES MotsRecherches

L'ensemble des fichiers texte brut contenant par jour et par mois les mots recherchés dans le DES en 2019 et 2020.

Ces fichiers sont créés à partir des logs du serveur web (avec le logiciel Apache) à l'aide de deux scripts shell (voir le répertoire /scripts)